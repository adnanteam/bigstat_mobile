/**
 * Created by Shakeel Ahmad on 5/17/2017.
 */
angular.module('starter.controllers')
  .controller('LoginCtrl', function ($scope,$rootScope,$state,$timeout,AppService){
    var accessToken=AppService.getToken();
    if(accessToken && accessToken!==null){
      $state.go('tab.home');
    }
    function displayError(){
      $scope.isError=true;
      $timeout(function(){
        $scope.isError=false;
      },3000);
    }
    $scope.user={};
    $scope.isError=false;
    $scope.login=function(){
      if($scope.user.username && $scope.user.username!=='' &&
        $scope.user.password && $scope.user.password!==''){
        AppService.login($scope.user).then(function(token){
          if(token===true){
            $state.go('tab.home');
          }else{
            displayError();
          }
        }).catch(function (err) {
          displayError();
        })
      }

    };

  });
