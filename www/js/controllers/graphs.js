/**
 * Created by Shakeel Ahmad on 5/17/2017.
 */
angular.module('starter.controllers')
  .controller('GraphsCtrl', function ($filter,$ionicPopup,$ionicPopover,$rootScope,$timeout,
      $cordovaGeolocation,$q,$scope,$state,GeneralService,AppService,AppointmentService,DashboardService) {
    $scope.selectList=function(index){
      if(AppService.isCurrentUserManager() && $rootScope.userData.selectedUserIndex!==index){
        $rootScope.userData.selectedUserIndex=index;
        $rootScope.userData.selectedUser=$rootScope.userData.list_items[$rootScope.userData.selectedUserIndex];
        AppService.setUserDataItem(JSON.stringify($rootScope.userData.selectedUser));
        $timeout(function () {
          $scope.closePopover();
        },200);
      }
    };


    //my-proportion-list-modal//
    $ionicPopover.fromTemplateUrl('templates/my-proportions-list-modal.html', {
      scope: $scope
    }).then(function(popover) {
      $scope.popover = popover;
    });
    GeneralService.initiatePopOverFunctions($scope);

    $scope.nextClick=function(){
      if($scope.monthDashboardData.length){
        $('.carousel').carousel('next');
        $scope.currentWeekIndex++;
        if($scope.currentWeekIndex > $scope.monthDashboardData.length-1){
          $scope.currentWeekIndex=0;
        }
        setCurrentMonth()
      }

    };

    $scope.prevClick=function(){
      if($scope.monthDashboardData.length) {
        $('.carousel').carousel('prev');
        $scope.currentWeekIndex--;
        if ($scope.currentWeekIndex < 0) {
          $scope.currentWeekIndex = $scope.monthDashboardData.length - 1;
        }
        setCurrentMonth();
      }
    };

    $scope.getAppointmentHeight=function(){
      var maxValue=_.maxBy($scope.monthDashboardData,function(item){
        return item.data.length;
      });
      if(maxValue>4){
        return "height:'"+maxValue*80+"px'";
      }else{
        return "height:'300px'";
      }
    };

    function setCurrentMonth(){
      $scope.currentWeekData=$scope.monthDashboardData[$scope.currentWeekIndex];
    }
    function setMapData(dataArray){
      if(dataArray && dataArray.length){
        var latLng = new google.maps.LatLng(dataArray[0].clientLatitude, dataArray[1].clientLongitude);
        var mapOptions = {
          center: latLng,
          zoom: 17,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        $scope.map = new google.maps.Map(document.getElementById("graph-map"), mapOptions);
        $timeout(function () {
          _.map(dataArray,function (item) {
            var loc_latlng = new google.maps.LatLng(item.clientLatitude, item.clientLongitude);
            var marker = new google.maps.Marker({
              position: loc_latlng,
              map: $scope.map
            });

          });
        },1000);
      }

    }
    function getAppointmentSetByWeek(monthData){
      var promises=[];
      _.forEach(monthData,function (item) {
        promises.push(AppointmentService.getAppointmentSetByWeek(item.startDate.split('T')[0],item.endDate.split('T')[0]));
      });
      $q.all(promises).then(function(weeks){
        if(weeks.length){
          for(var v=0;v<monthData.length;v++){
            setAppointmentData(weeks[v].data.appResumeSetByWeek[0].appResumeByWeek,monthData[v]);
          }
          setCurrentMonth();
          var maxWeekData=_.maxBy($scope.monthDashboardData,function(x){
            return x.data.length;
          });
          $timeout(function () {
            $('.carousel.carousel-slider').carousel({fullWidth: true});
            $timeout(function () {
              $('.carousel.carousel-slider').css("height", maxWeekData.data.length*90);
            },200);
          },200);
        }
      });
    }
    $scope.monthDashboardData=[];
    $scope.currentWeekIndex=0;
    function setAppointmentData(dataArray,origData){
      if(dataArray.length){
        var thisWeekData=_.map(dataArray, function(item){
          var itemDate=item.date.split("-"), currentDate=new Date(), isCurrentDay=false;
          currentDate=$filter('date')(new Date(currentDate), 'yyyy-MM-dd');
          if(parseInt(currentDate.split("-").pop())===parseInt(_.take(itemDate[itemDate.length-1],2).join(''))){
            isCurrentDay=true;
          }
          item.dateData={
            year:parseInt(itemDate[0]),
            monthNumber:parseInt(itemDate[1]),
            day:parseInt(_.take(itemDate[itemDate.length-1],2).join('')),
            month:GeneralService.getMonthString(parseInt(itemDate[1])),
            isCurrentDay:isCurrentDay
          };
          return item;
        });
        var weekStartDate=origData.startDate.split("-"), weekEndDate=origData.endDate.split("-");
        $scope.monthDashboardData.push({
          data:thisWeekData,
          weekDate:{
            startDay:parseInt(_.take(weekStartDate[weekStartDate.length-1],2).join('')),
            endDay:parseInt(_.take(weekEndDate[weekStartDate.length-1],2).join('')),
            thisMonth:GeneralService.getMonthString(parseInt(weekEndDate[1]))
          }
        });
      }
    }

    function getAppSetByDayAsWeek(){
      var day=new Date();
      AppointmentService.getAppSetByDayAsWeek(day).then(function(data){
        $scope.appointmentWeekData=data.data.appResumeSetByWeek[0].appResumeByWeek;
        setMapData($scope.appointmentWeekData);
      }).catch(function (err) {
        console.log(err);
      })
    }
    function getDashboardByMonthByUser(){
      var day=new Date();
      DashboardService.getDashboardByMonthByUser(day).then(function(data){
        getAppointmentSetByWeek(data.data.saleBySellerCADoneTotalSet);
      }).catch(function (err) {
        console.log(err);
      })
    }

    function getData(){
      getDashboardByMonthByUser();
      // getAppointmentSetByWeek();
      getAppSetByDayAsWeek();
    }
    var accessToken=AppService.getToken();
    if(accessToken && accessToken!==null){
      getData();
    }
  });
