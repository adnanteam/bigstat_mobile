/**
 * Created by Shakeel Ahmad on 5/17/2017.
 */
angular.module('starter.controllers')
  .controller('EventCtrl', function ($scope,$state,$timeout,$rootScope,$ionicPopover,GeneralService,
                                     AppointmentService,AppService,DashboardService){
    //my-proportion-list-modal//
    $ionicPopover.fromTemplateUrl('templates/my-proportions-list-modal.html', {
      scope: $scope
    }).then(function(popover) {
      $scope.popover = popover;
    });


    $scope.selectedItemIndex=1;
    $scope.appoinmentsList=GeneralService.getAppointmentsList();
    $scope.selectedAppointment=$scope.appoinmentsList[$scope.selectedItemIndex];

    GeneralService.initiatePopOverFunctions($scope);

    $scope.selectList=function(index){
      if(AppService.isCurrentUserManager() && $rootScope.userData.selectedUserIndex!==index){
        $rootScope.userData.selectedUserIndex=index;
        $rootScope.userData.selectedUser=$rootScope.userData.list_items[$rootScope.userData.selectedUserIndex];
        AppService.setUserDataItem(JSON.stringify($rootScope.userData.selectedUser));
        $timeout(function () {
          $scope.closePopover();
        },200);
      }
    };
    $scope.getAppointments=function(){
      $state.go('appointments')
    };


    function setMapData(dataArray){
      if(dataArray.length){
        var latLng = new google.maps.LatLng(dataArray[0].clientLatitude, dataArray[1].clientLongitude);
        var mapOptions = {
          center: latLng,
          zoom: 17,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        $scope.map = new google.maps.Map(document.getElementById("event-map"), mapOptions);
        $timeout(function () {
          _.map(dataArray,function (item) {
            var loc_latlng = new google.maps.LatLng(item.clientLatitude, item.clientLongitude);
            var marker = new google.maps.Marker({
              position: loc_latlng,
              map: $scope.map
            });
          });
        },1000);
      }
    }
    function getAppointmentSetByWeek(){
      var startweek=new Date();
      var endweek=new Date();
      AppointmentService.getAppointmentSetByWeek(startweek,endweek).then(function(data){
        $scope.appointmentWeekData=data.data.appResumeSetByWeek[0].appResumeByWeek;
        setMapData($scope.appointmentWeekData)
      }).catch(function (err) {
        console.log(err);
      })
    }

    $scope.chartData={
      maxValue:0
    };

    function initiateChartCarousel(chartData){
      GeneralService.startBarChart('high-chart-line-basic-appointment',chartData);
      $timeout(function(){
        $('.highcharts-container svg').attr("height", "330");
      });
    }

    function setChartData(monthData){
      var v1=_.map(monthData.saleBySellerCADoneTotalSet,function(item){
        return {nbSaleTotal:item.nbSaleTotal};
      }), v2=_.map(monthData.sellerAppBySellerHandledRateSet,function(item){
        return {
          nbAppHandled:item.nbAppHandled,
          nbAppCanceled:item.nbAppCanceled,
          nbAppAbsent:item.nbAppAbsent,
          nbAppRescheduled:item.nbAppRescheduled,
          nbNegoc:item.nbNegoc,
          nbClosed:item.nbClosed
        };
      });
      $scope.chartData.rawChartData=_.merge(v1,v2);
      mapChartData($scope.chartData.rawChartData)
    }
    function mapChartData(rawData){
      if(rawData.length){
        $scope.chartData.seriesData=_.map(rawData,function (item) {
          return item[$scope.selectedAppointment.key];
        });
        $scope.chartData.maxValue=_.max($scope.chartData.seriesData);
        $scope.chartData.weeksData=[];
        for(var v=0;v<rawData.length ;v++){
          $scope.chartData.weeksData.push("W"+(v+1));
        }
        initiateChartCarousel($scope.chartData);
      }
    }
    function setCurrentMonth(date){
      var year=parseInt(date.split("-")[0]),
      month=parseInt(date.split("-")[1]),
      day=parseInt(_.take(date.split("-").pop(),2).join(''));
      $scope.currentDateObject={
        year:year,
        monthNumber:month,
        day:day,
        month:GeneralService.getMonthString(month)
      };
    }
    function getDashboardByMonthByUser(){
      var day=new Date();
      DashboardService.getDashboardByMonthByUser(day).then(function(data){
        setChartData(data.data);
        setCurrentMonth(data.data.startDate);
      }).catch(function (err) {
        console.log(err);
      })
    }
    function getData(){
      getAppointmentSetByWeek();
      getDashboardByMonthByUser();
    }
    var accessToken=AppService.getToken();
    if(accessToken && accessToken!==null){
      getData();
    }
  });
