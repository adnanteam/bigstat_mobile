/**
 * Created by Shakeel Ahmad on 5/17/2017.
 */
angular.module('starter.controllers')
  .controller('CalenderCtrl', function ($filter,$q,$scope,$state,$ionicPopover,$timeout,$rootScope,
    AppService,DashboardService,AppointmentService,GeneralService){
    function getDashboardByYearByUser(){
      var day=new Date();
      DashboardService.getDashboardByYearByUser(day).then(function(data){
        getLineChartData(data.data.saleBySellerCADoneTotalSet);
        getCurrentMonthDataFromYear(data.data);
      }).catch(function (err) {
        console.log(err);
      })
    }
    function getDashboardByDayByUser(){
      var day=new Date();
      DashboardService.getDashboardByDayByUser(day).then(function(data){
        $scope.todayDashboardData=data.data;
      }).catch(function (err) {
        console.log(err);
      })
    }
    function getDashboardByMonthByUser(){
      var day=new Date();
      DashboardService.getDashboardByMonthByUser(day).then(function(data){
        getCurrentWeekDataFromYear(data.data);
        getAppointmentSetByWeek(data.data.saleBySellerCADoneTotalSet);
      }).catch(function (err) {
        console.log(err);
      })
    }

    $ionicPopover.fromTemplateUrl('templates/my-proportions-list-modal.html', {
      scope: $scope
    }).then(function(popover) {
      $scope.popover = popover;
    });

    GeneralService.initiatePopOverFunctions($scope);

    $scope.selectList=function(index){
      if(AppService.isCurrentUserManager() && $rootScope.userData.selectedUserIndex!==index){
        $rootScope.userData.selectedUserIndex=index;
        $rootScope.userData.selectedUser=$rootScope.userData.list_items[$rootScope.userData.selectedUserIndex];
        AppService.setUserDataItem(JSON.stringify($rootScope.userData.selectedUser));
        $timeout(function () {
          $scope.closePopover();
        },200);
      }
    };



    function nextClick(){
      $('.carousel').carousel('next');
    }

    function prevClick(){
      $('.carousel').carousel('prev');
    }
    $scope.nextClick=nextClick;

    $scope.prevClick=prevClick;

    function nextClickMission(){

      if($scope.monthDashboardData.length){
        $('.carousel-slider-appointment').carousel('next');
        $scope.currentWeekIndex++;
        if($scope.currentWeekIndex > $scope.monthDashboardData.length-1){
          $scope.currentWeekIndex=0;
        }
        setCurrentMonth()
      }
    }

    function prevClickMission(){

      if($scope.monthDashboardData.length) {
        $('.carousel-slider-appointment').carousel('prev');
        $scope.currentWeekIndex--;
        if ($scope.currentWeekIndex < 0) {
          $scope.currentWeekIndex = $scope.monthDashboardData.length - 1;
        }
        setCurrentMonth();
      }
    }
    $scope.nextClickMission=nextClickMission;

    $scope.prevClickMission=prevClickMission;

    $scope.viewDetails=function () {
      $state.go('tab.equal')
    };


    function getRevenueData(list){
      return _.map(list,function(item){
        return item.tenuCAHT;
      })
    }
    function getBillData(list){
      return _.map(list,function(item){
        return item.tenuConfirmedandPayedCAHT;
      })
    }
    function getCancelledData(list){
      return _.map(list,function(item){
        return item.canceledCAHT;
      })
    }
    function getMaxColumn(a,b,c){
      var list=[];
      list.push(a);
      list.push(b);
      list.push(c);
      return _.max(list);
    }
    function getColumnData(list1,list2,list3,length){
      var data=[];
      for(var v=0;v<length;v++){
        data.push(getMaxColumn(list1[v],list2[v],list3[v]));
      }
      return data;
    }
    function getLineChartData(list){
      var totalData={};
      totalData.revenueData=getRevenueData(list);
      totalData.billData=getBillData(list);
      totalData.cancelledData=getCancelledData(list);
      totalData.columnData=getColumnData(totalData.revenueData,totalData.billData,totalData.cancelledData,totalData.revenueData.length);
      totalData.yearsMonth=GeneralService.getYearsMonth(totalData.revenueData.length);
      GeneralService.startLineChart('high-chart-line-basic-calender',totalData);
      $('.carousel.carousel-slider').carousel({fullWidth: true});
      $('.carousel-missions.carousel-slider').carousel({fullWidth: true});
      $timeout(function(){
        $('.highcharts-container svg').attr("height", "400");
      });
    }

    function getCurrentWeekDataFromYear(monthData){
      var day=new Date();
      day=$filter('date')(new Date(day), 'yyyy-MM-dd');
      var currentWeek=_.find(monthData.saleBySellerCADoneTotalSet,function(week){
        var startDay=parseInt(_.take(week.startDate.split("-").pop(),2).join(''));
        var endDay=parseInt(_.take(week.endDate.split("-").pop(),2).join(''));
        return parseInt(day.split("-")[2])>=startDay && parseInt(day.split("-")[2])<=endDay
      });
      if(currentWeek){
        $scope.currentWeekSalesData=setDateData(currentWeek);
      }

    }

    function getAppointmentSetByWeek(monthData){
      var promises=[];
      _.forEach(monthData,function (item) {
        promises.push(AppointmentService.getAppointmentSetByWeek(item.startDate.split('T')[0],item.endDate.split('T')[0]));
      });
      $q.all(promises).then(function(weeks){
        if(weeks.length){
          for(var v=0;v<monthData.length;v++){
            setSaleAppointmentData(weeks[v].data.appResumeSetByWeek[0].appResumeByWeek,monthData[v]);
          }
          setCurrentMonth();
          var maxWeekData=_.maxBy($scope.monthDashboardData,function(x){
            return x.data.length;
          });

          $timeout(function () {
            $('.carousel.carousel-slider-appointment').carousel({fullWidth: true});
            $timeout(function () {
              $('.carousel.carousel-slider-appointment').css("height", maxWeekData.data.length*90);
            },200);
          },200);
        }
      });
    }
    function setCurrentMonth(){
      $scope.currentWeekData=$scope.monthDashboardData[$scope.currentWeekIndex];
    }
    $scope.monthDashboardData=[];
    $scope.currentWeekIndex=0;
    function setSaleAppointmentData(dataArray,origData){
      if(dataArray.length){
        var thisWeekData=_.map(dataArray, function(item){
          var itemDate=item.date.split("-"), currentDate=new Date(), isCurrentDay=false;
          currentDate=$filter('date')(new Date(currentDate), 'yyyy-MM-dd');
          if(parseInt(currentDate.split("-").pop())===parseInt(_.take(itemDate[itemDate.length-1],2).join(''))){
            isCurrentDay=true;
          }
          item.dateData={
            year:parseInt(itemDate[0]),
            monthNumber:parseInt(itemDate[1]),
            day:parseInt(_.take(itemDate[itemDate.length-1],2).join('')),
            month:GeneralService.getMonthString(parseInt(itemDate[1])),
            isCurrentDay:isCurrentDay
          };
          return item;
        });
        var weekStartDate=origData.startDate.split("-"), weekEndDate=origData.endDate.split("-");
        $scope.monthDashboardData.push({
          data:thisWeekData,
          weekDate:{
            startDay:parseInt(_.take(weekStartDate[weekStartDate.length-1],2).join('')),
            endDay:parseInt(_.take(weekEndDate[weekStartDate.length-1],2).join('')),
            thisMonth:GeneralService.getMonthString(parseInt(weekEndDate[1]))
          }
        });
      }
    }
    function setDateData(data){
      var year=parseInt(data.startDate.split("-")[0]),
        month=parseInt(data.startDate.split("-")[1]),
        day=parseInt(_.take(data.startDate.split("-").pop(),2).join('')),
        currentDate=new Date(),
        isCurrentDay=false;
      currentDate=$filter('date')(new Date(currentDate), 'yyyy-MM-dd');
      var currentDateDay=parseInt(_.take(currentDate.split("-").pop(),2).join(''));
      if(currentDateDay===day){
        isCurrentDay=true;
      }
      data.dateData={
        year:year,
        monthNumber:month,
        day:day,
        month:GeneralService.getMonthString(month),
        isCurrentDay:isCurrentDay
      };
      return data;
    }
    function getCurrentMonthDataFromYear(yearData){
      var day=new Date();
      day=$filter('date')(new Date(day), 'yyyy-MM-dd');
      var currentMonth=_.find(yearData.saleBySellerCADoneTotalSet,function(month){
        return month.startDate.split("-")[1]===day.split("-")[1];
      });
      if(currentMonth){
        $scope.currentMonthSalesData=setDateData(currentMonth);
      }

    }


    function getData(){
      getDashboardByMonthByUser();
      getDashboardByYearByUser();
      getDashboardByDayByUser();
    }

    var accessToken=AppService.getToken();
    if(accessToken && accessToken!==null){
      getData();
    }
  });
