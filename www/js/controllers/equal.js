/**
 * Created by Shakeel Ahmad on 5/17/2017.
 */
angular.module('starter.controllers')
  .controller('EqualCtrl', function ($scope,$state,ProfileService) {
    $scope.backClick=function(){
      $state.go('tab.event');
    };
    function getNbSalesCaByUser(){
      var startday=new Date();
      var endday=new Date();
      ProfileService.getNbSalesCaByUser(startday,endday).then(function(data){
        console.log(data);
        $scope.nbData=data.data.nbSalesCaUsersList;
        $scope.nbTotal={
          nbSales:_.sumBy($scope.nbData,'nbSales'),
          cAdone:_.sumBy($scope.nbData,'cAdone')
        }
      }).catch(function (err) {
        console.log(err);
      })
    }
    getNbSalesCaByUser();
  });
