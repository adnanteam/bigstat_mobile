/**
 * Created by Shakeel Ahmad on 5/17/2017.
 */
angular.module('starter.controllers', [])
  .run(function($rootScope, $ionicScrollDelegate) {
    $rootScope.$on("$locationChangeSuccess", function(){
      $ionicScrollDelegate.scrollTop();
    })
  })
  .controller('HomeCtrl', function ($scope,$state,$rootScope,$filter,$ionicModal,$ionicPopover,$timeout,UserService,
                                    GeneralService,AppService,DashboardService,AppointmentService,_){
    $ionicPopover.fromTemplateUrl('templates/my-proportions-list-modal.html', {
      scope: $scope
    }).then(function(popover) {
      $scope.popover = popover;
    });

    GeneralService.initiatePopOverFunctions($scope);

    $scope.selectList=function(index){
      if(AppService.isCurrentUserManager() && $rootScope.userData.selectedUserIndex!==index){
        $rootScope.userData.selectedUserIndex=index;
        $rootScope.userData.selectedUser=$rootScope.userData.list_items[$rootScope.userData.selectedUserIndex];
        AppService.setUserDataItem(JSON.stringify($rootScope.userData.selectedUser));
        getDashBoardAppStatByWeekByUser(new Date());
        $timeout(function () {
          $scope.closePopover();
        },200);
      }
    };

    $scope.nextDay=function(date){
      var d = new Date(date);
      var newDate=d.setDate(d.getDate()+1);
      getDashBoardAppStatByWeekByUser(new Date(newDate))
    };

    $scope.prevDay=function(date){
      var d = new Date(date);
      var newDate=d.setDate(d.getDate()-1);
      getDashBoardAppStatByWeekByUser(new Date(newDate))
    };

    function getMyProportion(event) {

      $scope.isChart=true;
      $scope.list_style={
        'border-bottom': 'none',
        'font-weight': 'none'
      };
      $scope.my_proportion_stye={
        'border-bottom': '3px solid',
        'font-weight': 'bold',
        'background-image': 'url(img/down-arrow.png)'
      };
      $scope.openPopover(event);
      $timeout(function () {
        var svgs = $("svg");
        if(svgs && svgs.length){
          var realHeight = $(svgs[0]).attr("viewBox").split(' ').pop();
          if($(svgs[0]).attr("height")!==realHeight){
            $(svgs[0]).attr("height",realHeight);
            $(svgs[1]).attr("height",realHeight);
          }
        }
      },100);
    }

    function getList(event) {
      $scope.isChart=false;
      $scope.list_style={
        'border-bottom': '3px solid',
        'font-weight': 'bold',
        'background-image': 'url(img/down-arrow.png)'
      };
      $scope.my_proportion_stye={
        'border-bottom': 'none',
        'font-weight': 'none'
      };
      $scope.openPopover(event);
    }

    $scope.nextClick= function(){
      $('.carousel').carousel('next');
    };

    $scope.prevClick=function(){
      $('.carousel').carousel('prev');
    };

    $scope.isChart=true;

    $scope.getList=getList;

    $scope.getMyProportion=getMyProportion;


    $scope.logout=function(){
      AppService.logout();
      $timeout(function(){
        $state.go('login');
      },500);
    };


    $scope.$on('$ionicView.enter', function(e) {
      $scope.my_proportion_stye={'border-bottom': '3px solid', 'font-weight': 'bold', 'background-image': 'url(img/down-arrow.png)'};
    });

    function loginUser(){
      AppService.login().then(function(token){
        if(token===true){
          getData();
        }
      }).catch(function (err) {
        console.log(err);
      })
    }

    $rootScope.userData={};
    function getListOfUsersForAgendaByUser(){
      UserService.getListOfUsersForAgendaByUser().then(function(data){
        // console.log(data);
        var list_data=[];
        list_data=_.concat(list_data,data.data.sellerList);
        list_data=_.concat(list_data,data.data.callopList);
        list_data=_.concat(list_data,data.data.technicianList);
        $rootScope.userData.list_items=list_data;
        $rootScope.userData.selectedUserIndex=0;
        $rootScope.userData.selectedUser=list_data[$rootScope.userData.selectedUserIndex];
        AppService.setUserDataItem(JSON.stringify($rootScope.userData.selectedUser));
        getDashBoardAppStatByWeekByUser(new Date());
      }).catch(function (err) {
        console.log(err);
      })
    }

    function getAppointmentSetListForSearch(){
      var startweek=new Date();
      var endweek=new Date();
      AppointmentService.getAppointmentSetListForSearch(startweek,endweek).then(function(data){
        $scope.listDataItems=data.data.appResumeSet;
      }).catch(function (err) {
        console.log(err);
      })
    }
    function getDateObject(date){
      var dateSplits=date.split('-');
      return {
        date:date,
        year:parseInt(dateSplits[0]),
        monthNumber:parseInt(dateSplits[1]),
        month:GeneralService.getMonthString(parseInt(dateSplits[1])),
        day:parseInt(dateSplits[2])
      };
    }
    function setTodayDateObj(date){
      $scope.todayDate=date;
      var thisDate= $filter('date')(new Date(date), 'yyyy-MM-dd'),
        currentDate = $filter('date')(new Date(), 'yyyy-MM-dd');
      if(currentDate === thisDate){
        $scope.todayDateText='Aujourd’hui'
      }else{
        var dateObj=getDateObject(thisDate);
        $scope.todayDateText= dateObj.month + " "+ dateObj.day + ", "+ dateObj.year;
      }
    }
    function getDashBoardAppStatByWeekByUser(date){
      setTodayDateObj(date);
      DashboardService.getDashBoardAppStatByWeekByUser(date, date).then(function(data){
        $scope.dashboardChartData=data.data;
        initiateChartCarousel();
      }).catch(function (err) {
        console.log(err);
      })
    }
    function initiateChartCarousel(){
      GeneralService.startPieChart("pie-chart-1",{
        data: [
          ['Exploited', $scope.dashboardChartData.appExploitedPercent],
          ['Non Exploited', $scope.dashboardChartData.appPercent]
        ]
      });
      GeneralService.startPieChart("pie-chart-2",{
        data: [
          ['Non Exploited', $scope.dashboardChartData.appPercent],
          ['Exploited', $scope.dashboardChartData.appExploitedPercent]
        ]
      });
      var carousel_slider=$('.carousel.carousel-slider');
      if($(carousel_slider[0]).hasClass('initialized')){
        $(carousel_slider[0]).removeClass('initialized')
      }
      carousel_slider.carousel({fullWidth: true});
    }
    function getData(){
      getListOfUsersForAgendaByUser();
      getAppointmentSetListForSearch();
    }




    var accessToken=AppService.getToken();
    if(accessToken && accessToken!==null){
      getData();
    }else{
      loginUser();
    }
  });
