/**
 * Created by Shakeel Ahmad on 5/17/2017.
 */
angular.module('starter.controllers')
  .controller('AppointmentCtrl', function ($q,$scope,$filter,$state,$rootScope,$ionicPopover,$timeout,
                                           GeneralService,AppService,DashboardService,ProfileService){
    $scope.selectList=function(index){
      if(AppService.isCurrentUserManager() && $rootScope.userData.selectedUserIndex!==index){
        $rootScope.userData.selectedUserIndex=index;
        $rootScope.userData.selectedUser=$rootScope.userData.list_items[$rootScope.userData.selectedUserIndex];
        AppService.setUserDataItem(JSON.stringify($rootScope.userData.selectedUser));
        // getDashBoardAppStatByWeekByUser(new Date());
        $timeout(function () {
          $scope.closePopover();
        },200);
      }
    };

    function getDateObject(date){
      var dateSplits=date.split('-');
      return {
        date:date,
        year:parseInt(dateSplits[0]),
        monthNumber:parseInt(dateSplits[1]),
        month:GeneralService.getMonthString(parseInt(dateSplits[1])),
        day:parseInt(dateSplits[2])
      };
    }
    function getNextMonth(todayDateObj){
      var dateObj={};
      if(todayDateObj.monthNumber===12){
        dateObj.date=(todayDateObj.year+1)+'-01-01';
        dateObj.year=(todayDateObj.year+1);
        dateObj.monthNumber=1;
        dateObj.month=GeneralService.getMonthString(1);
        dateObj.day=1;
      }else{
        if(todayDateObj.monthNumber<9){
          dateObj.date=(todayDateObj.year)+'-0'+(todayDateObj.monthNumber+1)+'-01';
          dateObj.year=(todayDateObj.year);
          dateObj.monthNumber=todayDateObj.monthNumber+1;
          dateObj.month=GeneralService.getMonthString(todayDateObj.monthNumber+1);
          dateObj.day=1;
        }else{
          dateObj.date=(todayDateObj.year)+'-'+(todayDateObj.monthNumber+1)+'-01';
          dateObj.year=(todayDateObj.year);
          dateObj.monthNumber=todayDateObj.monthNumber+1;
          dateObj.month=GeneralService.getMonthString(todayDateObj.monthNumber+1);
          dateObj.day=1;
        }
      }
      return dateObj;
    }
    function getPrevMonth(todayDateObj){
      var dateObj={};
      if(todayDateObj.monthNumber===1){
        dateObj.date=(todayDateObj.year-1)+'-12-01';
        dateObj.year=(todayDateObj.year-1);
        dateObj.monthNumber=12;
        dateObj.month=GeneralService.getMonthString(12);
        dateObj.day=1;
      }else{
        if(todayDateObj.monthNumber<11){
          dateObj.date=(todayDateObj.year)+'-0'+(todayDateObj.monthNumber-1)+'-01';
          dateObj.year=(todayDateObj.year);
          dateObj.monthNumber=todayDateObj.monthNumber-1;
          dateObj.month=GeneralService.getMonthString(todayDateObj.monthNumber-1);
          dateObj.day=1;
        }else{
          dateObj.date=(todayDateObj.year)+'-'+(todayDateObj.monthNumber-1)+'-01';
          dateObj.year=(todayDateObj.year);
          dateObj.monthNumber=todayDateObj.monthNumber-1;
          dateObj.month=GeneralService.getMonthString(todayDateObj.monthNumber-1);
          dateObj.day=1;
        }
      }
      return dateObj;
    }
    function getDashboardByMonthByUser(dateObj){
      DashboardService.getDashboardByMonthByUser(dateObj.date).then(function(data){
        $scope.monData=null;
        $scope.monData=data.data;
        $scope.monData.dateObj=dateObj;
        setMonthChartData($scope.monData)
      }).catch(function (err) {
        console.log(err);
      })
    }
    function getLastAppForUsers(){
      ProfileService.getLastAppForUsers().then(function(data){
        $scope.userListingData=data.data;
      }).catch(function (err) {
        console.log(err);
      })
    }
    function setMonthChartData(monthData){
      var v1=_.map(monthData.saleBySellerCADoneTotalSet,function(item){
        return {nbSaleTotal:item.nbSaleTotal};
      }), v2=_.map(monthData.sellerAppBySellerHandledRateSet,function(item){
        return {
          nbAppHandled:item.nbAppHandled,
          nbAppCanceled:item.nbAppCanceled,
          nbAppAbsent:item.nbAppAbsent,
          nbAppRescheduled:item.nbAppRescheduled,
          nbNegoc:item.nbNegoc,
          nbClosed:item.nbClosed
        };
      });
      monthData.rawChartData=_.merge(v1,v2);
      mapMonthChartData(monthData)
    }
    function mapMonthChartData(monthData){
      if(monthData.rawChartData.length){
        monthData.seriesData=_.map(monthData.rawChartData,function (item) {
          return item[$scope.selectedAppointment.key];
        });
        monthData.maxValue=_.max(monthData.seriesData);
        monthData.weeksData=[];
        for(var v=0;v<monthData.rawChartData.length ;v++){
          monthData.weeksData.push("W"+(v+1));
        }
        startBarChart(monthData);
      }
    }
    function startBarChart(monthData){
      GeneralService.startBarChart('bar-chart-appointments' , monthData);
      $timeout(function(){
        $timeout(function(){
          $('.highcharts-container svg').attr("height", "330");
        });
        $('.highcharts-container svg').attr("height", "330");
      });
    }
    function getFilteredDate(date){
      return $filter('date')(new Date(date), 'yyyy-MM-dd');
    }
    function getData(){
      getLastAppForUsers();
      getDashboardByMonthByUser(getDateObject(getFilteredDate(new Date())));
    }


    $scope.selectedItemIndex=1;
    $scope.appoinmentsList=GeneralService.getAppointmentsList();
    $scope.selectedAppointment=$scope.appoinmentsList[$scope.selectedItemIndex];

    $scope.selectPropList=function(index){
      $scope.selectedItemIndex=index;
      $scope.selectedAppointment=$scope.appoinmentsList[$scope.selectedItemIndex];
      $scope.monData.seriesData=_.map($scope.monData.rawChartData,function (item) {
        return item[$scope.selectedAppointment.key] || 0;
      });
      $scope.monData.maxValue=_.max($scope.monData.seriesData);
      startBarChart($scope.monData);
      $timeout(function () {
        $scope.closePopover();
      },500);
    };
    $scope.selectList=function(index){
      if(AppService.isCurrentUserManager() && $rootScope.userData.selectedUserIndex!==index){
        $rootScope.userData.selectedUserIndex=index;
        $rootScope.userData.selectedUser=$rootScope.userData.list_items[$rootScope.userData.selectedUserIndex];
        AppService.setUserDataItem(JSON.stringify($rootScope.userData.selectedUser));
        $timeout(function () {
          $scope.closePopover();
        },200);
      }
    };
    $scope.backClick=function(){
      $state.go('tab.event');
    };
    $scope.nextClick=function nextClick(){
      var dateObject = getNextMonth($scope.monData.dateObj);
      getDashboardByMonthByUser(dateObject);
    };

    $scope.prevClick=function prevClick(){
      var dateObject = getPrevMonth($scope.monData.dateObj);
      getDashboardByMonthByUser(dateObject);
    };

    $ionicPopover.fromTemplateUrl('templates/my-appointments-list-modal.html', {
      scope: $scope
    }).then(function(popover) {
      $scope.popover = popover;
    });

    $ionicPopover.fromTemplateUrl('templates/my-proportions-list-modal.html', {
      scope: $scope
    }).then(function(popover) {
      $scope.popover2 = popover;
    });
    GeneralService.initiatePopOverFunctions($scope);
    $scope.$on('$ionicView.enter', function() {
      var accessToken=AppService.getToken();
      if(accessToken && accessToken!==null){
        getData();
      }
    });

  });
