/**
 * Created by Shakeel Ahmad on 6/13/2017.
 */
angular.module('starter.services')
  .factory('CompanyService', function ($http,$filter,AppService){
    return {
      getCompany: function(id){
        if(AppService.isTokenAvailable()){
          var url=AppService.apiPrefix+'api/companies/'+id;
          return $http({
            url: url,
            method: "GET",
            headers: {
              'Content-Type': 'application/json',
              'Authorization':'Bearer '+AppService.getToken()
            }
          }).then(function (r) {
            return r;
          }).catch(function(err){
            return err;
          });
        }
        return null;
      }
    };
  });
