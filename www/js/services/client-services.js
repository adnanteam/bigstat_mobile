/**
 * Created by Shakeel Ahmad on 6/13/2017.
 */
angular.module('starter.services')
  .factory('ClientService', function ($http,$filter,AppService){
    return {
      getClient: function(id){
        if(AppService.isTokenAvailable()){
          var url=AppService.apiPrefix+'api/clients/'+id;
          return $http({
            url: url,
            method: "GET",
            headers: {
              'Content-Type': 'application/json',
              'Authorization':'Bearer '+AppService.getToken()
            }
          }).then(function (r) {
            return r;
          }).catch(function(err){
            return err;
          });
        }
        return null;
      }
    };
  });
