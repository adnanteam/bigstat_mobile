/**
 * Created by Shakeel Ahmad on 6/13/2017.
 */
angular.module('starter.services')
  .factory('AppService', function ($http){
    var service={
      apiPrefix:'http://mybigstat.com:8585/',
      login:function(user){
        var loginData;
        if(angular.isDefined(user)&&user.username&&user.password){
          loginData=user;
        }else{
          loginData={username:"testSeller1@test.com",password:"BigStat7Dev"}
        }
        var url=service.apiPrefix+'api/account/login';
        return $http({
          url: url,
          method: "POST",
          data: loginData,
          headers: { 'Content-Type': 'application/json'}
        }).then(function(response) {
          if(response && response.data && response.data.accessToken){
            service.setToken(response.data.accessToken);
            return service.authorized();
          }else{
            service.clearToken();
            return false;
          }
        }).catch(function(err){
          service.clearToken();
          return false;
        });
      },
      logout : function(){
        localStorage.removeItem('user-info');
        localStorage.removeItem('user-data');
        localStorage.removeItem('token');
      },
      authorized: function () {
        var url=service.apiPrefix+'api/users/userinfo';
        return $http({
          url: url,
          method: "GET",
          headers: { 'Content-Type': 'application/json',
            'Authorization':'Bearer '+service.getToken()}
        }).then(function(response) {
          localStorage.removeItem('user-data');
          localStorage.setItem('user-data', JSON.stringify(response));
          return true;
        }).catch(function(err){
          localStorage.removeItem('user-data');
          return false;
        });
      },
      isCurrentUserManager:function(){
        return JSON.parse(localStorage.getItem('user-data')).data.isManager;
      },
      setUserDataItem:function(userInfo){
        localStorage.removeItem('user-info');
        localStorage.setItem('user-info', userInfo);
      },
      isTokenAvailable:function(){
        var lReturn=false;
        var token=service.getToken();
        if(token&&token!==null){
          lReturn=true;
        }
        return lReturn;
      },
      setToken:function(token){
        localStorage.setItem('token', JSON.stringify(token));
      },
      getToken:function(){
        var token = JSON.parse(localStorage.getItem('token'));
        return token;
      },
      clearToken:function(){
        localStorage.removeItem('token');
      }
    };
    return service;
  });
