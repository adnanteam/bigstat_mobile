/**
 * Created by Shakeel Ahmad on 6/13/2017.
 */
angular.module('starter.services')
  .factory('UserService', function ($http,$filter,AppService){
    return {
      getListOfUsersForAgendaByUser: function(){
        if(AppService.isTokenAvailable()){
          var url=AppService.apiPrefix+'api/users/getListOfUsersForAgendaByUser';
          return $http({
            url: url,
            method: "GET",
            headers: {
              'Content-Type': 'application/json',
              'Authorization':'Bearer '+AppService.getToken()
            }
          }).then(function (r) {
            return r;
          }).catch(function(err){
            return err;
          });
        }
        return null;
      },
      getUser: function(){
        if(AppService.isTokenAvailable()){
          var url=AppService.apiPrefix+'api/users/'+JSON.parse(localStorage.getItem('user-info')).id;
          return $http({
            url: url,
            method: "GET",
            headers: {
              'Content-Type': 'application/json',
              'Authorization':'Bearer '+AppService.getToken()
            }
          }).then(function (r) {
            return r;
          }).catch(function(err){
            return err;
          });
        }
        return null;
      }
    };
  });
