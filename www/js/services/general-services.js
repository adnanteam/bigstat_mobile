/**
 * Created by Shakeel Ahmad on 6/13/2017.
 */
angular.module('starter.services')
  .factory('GeneralService', function ($http,AppService,$timeout){
    var service={
      appointmentsList:[
        {name:"Exploited appointment", key:"nbAppHandled"},
        {name:"Sales number", key:"nbSaleTotal"},
        {name:"Closed appointment number", key:"nbClosed"},
        {name:"Negotiation appointment number", key:"nbNegoc"},
        {name:"Report appointment number", key:"nbAppRescheduled"},
        {name:"Absent appointment number", key:"nbAppAbsent"},
        {name:"Cancelled  appointment", key:"nbAppCanceled"}
      ],
      logout:function($state){
        AppService.logout();
        $timeout(function(){
          $state.go('login');
        },500);
      },
      initiatePopOverFunctions:function($scope){
        $scope.openPopover = function($event) {
          $scope.popover.show($event);
          $timeout(function(){
            $('.highcharts-container svg').attr("height", "330");
          });
        };
        $scope.openPopover2 = function($event) {
          $scope.popover2.show($event);
          $timeout(function(){
            $('.highcharts-container svg').attr("height", "330");
          });
        };
        $scope.closePopover = function() {
          $scope.popover.hide();
          if($scope.popover2){
            $scope.popover2.hide();
          }
        };
        $scope.$on('$destroy', function() {
          $scope.popover.remove();
          if($scope.popover2){
            $scope.popover2.remove();
          }
        });
      },
      getAppointmentsList:function(){
        return service.appointmentsList;
      },
      getMonthString : function(n){
        if(n===1){
          return "Jan";
        }else if(n===2){
          return "Feb";
        }else if(n===3){
          return "Mar";
        }else if(n===4){
          return "Apr";
        }else if(n===5){
          return "May";
        }else if(n===6){
          return "June";
        }else if(n===7){
          return "July";
        }else if(n===8){
          return "Aug";
        }else if(n===9){
          return "Sep";
        }else if(n===10){
          return "Oct";
        }else if(n===11){
          return "Nov";
        }else if(n===12){
          return "Dec";
        }
      },
      getYearsMonth:function(length){
        var data=[];
        for(var v=0;v<=11;v++){
          if(v===0)
            data.push("Jan");
          if(v===1)
            data.push("Fev");
          if(v===1)
            data.push("Mar");
          if(v===3)
            data.push("Avr");
          if(v===4)
            data.push("Mai");
          if(v===5)
            data.push("Jun");
          if(v===6)
            data.push("Jul");
          if(v===7)
            data.push("Aug");
          if(v===8)
            data.push("Sep");
          if(v===9)
            data.push("Oct");
          if(v===10)
            data.push("Nov");
          if(v===11)
            data.push("Dec");
        }
        return data;
      },
      startLineChart:function(id,totalData){
        Highcharts.chart(id, {
          yAxis: {
            title: {
              text: ''
            }
          },
          legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
          },
          xAxis: {
            categories: totalData.yearsMonth
          },
          tooltip: {
            followTouchMove: false,
            enabled:false,
            animation: false
          },
          series: [
            {
              type: 'column',
              showInLegend: false,
              pointWidth: 50,
              color:  {
                linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                stops: [
                  [0, '#ffffff'],
                  [1, '#f2f2f2'],
                  [2, '#eaeaea']
                ]
              },
              data: totalData.columnData
            },
            {
              showInLegend: false,
              color: '#1E304A',
              marker: {
                symbol:'circle',
                fillColor: '#FFFFFF',
                lineWidth: 2,
                lineColor: '#cccccc'
              },
              data: totalData.revenueData
            }, {
              showInLegend: false,
              color: '#00D1FF',
              marker: {
                symbol:'circle',
                fillColor: '#FFFFFF',
                lineWidth: 2,
                lineColor: '#cccccc'
              },
              data: totalData.billData
            }, {
              showInLegend: false,
              color:'#FF7375',
              marker: {
                symbol:'circle',
                fillColor: '#FFFFFF',
                lineWidth: 2,
                lineColor: '#cccccc'
              },
              data: totalData.cancelledData
            }
          ]

        });
      },
      startPieChart:function(chartContainer,seriesData){
        var chart = new Highcharts.Chart({
          credits: {
            enabled: false
          },
          title: {
            text: null
          },
          chart: {
            renderTo: chartContainer,
            type: 'pie',
            margin: [0, 0, 0, 0],
            spacingTop: 0,
            spacingBottom: 0,
            spacingLeft: 0,
            spacingRight: 0,
            backgroundColor:'rgba(255, 255, 255, 0.1)'
          },
          plotOptions: {
            pie: {
              innerSize: '50%',
              size: 150,
              dataLabels: {
                enableMouseTracking: false
              },
              colors: [{
                linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                stops: [
                  [0, '#3F6DA1'],
                  [1, '#FAFBFD']
                ]
              }, {
                linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                stops: [
                  [0, '#FF7375'],
                  [1, '#FAFBFD']
                ]
              }]
            },
            series: {
              dataLabels: {
                enabled: true,
                distance: -20,
                format: '{point.y:.1f}%'
              }
            }
          },
          tooltip: {
            followTouchMove: false,
            enabled:false,
            animation: false
          },
          series: [
            seriesData
          ]
        },function(chart) {
          var xpos = '50%';
          var ypos = '53%';
          var circleradius = 80;
        });
      },
      startBarChart:function(id,data){
        Highcharts.chart(id, {
          height: 330,
          width:412,
          yAxis: {
            min: 0,
            max: data.maxValue,
            title: {
              text: ''
            }
          },
          legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
          },
          xAxis: {
            categories: data.weeksData
          },
          plotOptions: {
            series: {
              borderRadius:10
            }
          },
          series: [
            {
              type: 'column',
              showInLegend: false,
              pointWidth: 45,
              color:  {
                linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                stops: [
                  [0, '#efbef2'],
                  [1, '#efdff0']
                ]
              },
              data: data.seriesData
            }
          ]

        });
      }
    };
    return service;
  });
