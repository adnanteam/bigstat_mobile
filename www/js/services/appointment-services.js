/**
 * Created by Shakeel Ahmad on 6/13/2017.
 */
angular.module('starter.services')
  .factory('AppointmentService', function ($http,$filter,AppService){
    return {
      getTechAppSetByDay:function(day){
        if(AppService.isTokenAvailable()){
          var _date = $filter('date')(new Date(day), 'yyyy-MM-dd');
          var url=AppService.apiPrefix+'api/appointments/getTechAppSetByDay/' + _date;
          return $http({
            url: url,
            method: "GET",
            headers: {
              'Content-Type': 'application/json',
              'Authorization':'Bearer '+AppService.getToken()
            }
          }).then(function (r) {
            return r;
          }).catch(function(err){
            return err;
          });
        }
        return null;
      },
      getAppSetByDayForAMonth:function(month){
        if(AppService.isTokenAvailable()){
          var _date = $filter('date')(new Date(month), 'yyyy-MM-dd');
          var url=AppService.apiPrefix+'api/appointments/getAppSetByDayForAMonth/' + _date;
          return $http({
            url: url,
            method: "GET",
            headers: {
              'Content-Type': 'application/json',
              'Authorization':'Bearer '+AppService.getToken()
            }
          }).then(function (r) {
            return r;
          }).catch(function(err){
            return err;
          });
        }
        return null;
      },
      getAppSetByDayAsWeek:function(day){
        if(AppService.isTokenAvailable()){
          var _date = $filter('date')(new Date(day), 'yyyy-MM-dd');
          var url=AppService.apiPrefix+'api/appointments/getAppSetByDayAsWeek/' + _date;
          return $http({
            url: url,
            method: "GET",
            headers: {
              'Content-Type': 'application/json',
              'Authorization':'Bearer '+AppService.getToken()
            }
          }).then(function (r) {
            return r;
          }).catch(function(err){
            return err;
          });
        }
        return null;
      },
      getNbAppByDayForMonth:function(day){
        if(AppService.isTokenAvailable()){
          var _date = $filter('date')(new Date(day), 'yyyy-MM-dd');
          var url=AppService.apiPrefix+'api/appointments/getNbAppByDayForMonth/' + _date;
          return $http({
            url: url,
            method: "GET",
            headers: {
              'Content-Type': 'application/json',
              'Authorization':'Bearer '+AppService.getToken()
            }
          }).then(function (r) {
            return r;
          }).catch(function(err){
            return err;
          });
        }
        return null;
      },
      getNbAppHandledByDayForMonth:function(day){
        if(AppService.isTokenAvailable()){
          var _date = $filter('date')(new Date(day), 'yyyy-MM-dd');
          var url=AppService.apiPrefix+'api/appointments/getNbAppHandledByDayForMonth/' + _date;
          return $http({
            url: url,
            method: "GET",
            headers: {
              'Content-Type': 'application/json',
              'Authorization':'Bearer '+AppService.getToken()
            }
          }).then(function (r) {
            return r;
          }).catch(function(err){
            return err;
          });
        }
        return null;
      },
      getNbAppByMonthForYear:function(year){
        if(AppService.isTokenAvailable()){
          var _date = $filter('date')(new Date(year), 'yyyy-MM-dd');
          var url=AppService.apiPrefix+'api/appointments/getNbAppByMonthForYear/' + _date;
          return $http({
            url: url,
            method: "GET",
            headers: {
              'Content-Type': 'application/json',
              'Authorization':'Bearer '+AppService.getToken()
            }
          }).then(function (r) {
            return r;
          }).catch(function(err){
            return err;
          });
        }
        return null;
      },
      getNbAppHandledByMonthForYear:function(year){
        if(AppService.isTokenAvailable()){
          var _date = $filter('date')(new Date(year), 'yyyy-MM-dd');
          var url=AppService.apiPrefix+'api/appointments/getNbAppHandledByMonthForYear/' + _date;
          return $http({
            url: url,
            method: "GET",
            headers: {
              'Content-Type': 'application/json',
              'Authorization':'Bearer '+AppService.getToken()
            }
          }).then(function (r) {
            return r;
          }).catch(function(err){
            return err;
          });
        }
        return null;
      },
      getAppointmentById:function(id){
        if(AppService.isTokenAvailable()){
          var url=AppService.apiPrefix+'api/appointments/' + id;
          return $http({
            url: url,
            method: "GET",
            headers: {
              'Content-Type': 'application/json',
              'Authorization':'Bearer '+AppService.getToken()
            }
          }).then(function (r) {
            return r;
          }).catch(function(err){
            return err;
          });
        }
        return null;
      },
      getAppSetByClient:function(id){
        if(AppService.isTokenAvailable()){
          var url=AppService.apiPrefix+'api/appointments/getAppSetByClient/' + id;
          return $http({
            url: url,
            method: "GET",
            headers: {
              'Content-Type': 'application/json',
              'Authorization':'Bearer '+AppService.getToken()
            }
          }).then(function (r) {
            return r;
          }).catch(function(err){
            return err;
          });
        }
        return null;
      },
      getAppStatGroupByMonth:function(){
        if(AppService.isTokenAvailable()){
          var url=AppService.apiPrefix+'api/appointments/getAppStatGroupByMonth';
          return $http({
            url: url,
            method: "GET",
            headers: {
              'Content-Type': 'application/json',
              'Authorization':'Bearer '+AppService.getToken()
            }
          }).then(function (r) {
            return r;
          }).catch(function(err){
            return err;
          });
        }
        return null;
      },

      getAppStatGroupByWeek:function(month){
        if(AppService.isTokenAvailable()){
          var _date = $filter('date')(new Date(month), 'yyyy-MM-dd');
          var url=AppService.apiPrefix+'api/appointments/getAppStatGroupByWeek/' + _date;
          return $http({
            url: url,
            method: "GET",
            headers: {
              'Content-Type': 'application/json',
              'Authorization':'Bearer '+AppService.getToken()
            }
          }).then(function (r) {
            return r;
          }).catch(function(err){
            return err;
          });
        }
        return null;
      },
      getAppSetByDay:function(day){
        if(AppService.isTokenAvailable()){
          var _date = $filter('date')(new Date(day), 'yyyy-MM-dd');
          var url=AppService.apiPrefix+'api/appointments/getAppSetByDay/' + _date;
          return $http({
            url: url,
            method: "GET",
            headers: {
              'Content-Type': 'application/json',
              'Authorization':'Bearer '+AppService.getToken()
            }
          }).then(function (r) {
            return r;
          }).catch(function(err){
            return err;
          });
        }
        return null;
      },
      getAppointmentsByDayForTechnician: function (day) {
        if(AppService.isTokenAvailable()){
          var _date = $filter('date')(new Date(day), 'yyyy-MM-dd');
          var url=AppService.apiPrefix+'api/appointments/getAppointmentsByDayForTechnician/'+_date;
          return $http({
            url: url,
            method: "GET",
            headers: {
              'Content-Type': 'application/json',
              'Authorization':'Bearer '+AppService.getToken()
            }
          }).then(function (r) {
            return r;
          }).catch(function(err){
            return err;
          });
        }
        return null;
      },
      getAppointmentSetByWeek: function (startweek, endweek) {
        if(AppService.isTokenAvailable()){
          var _startweek = $filter('date')(new Date(startweek), 'yyyy-MM-dd');
          var _endweek = $filter('date')(new Date(endweek), 'yyyy-MM-dd');
          var url=AppService.apiPrefix+'api/appointments/getAppointmentByWeek/'+_startweek+'/'+_endweek;
          return $http({
            url: url,
            method: "GET",
            headers: {
              'Content-Type': 'application/json',
              'Authorization':'Bearer '+AppService.getToken()
            }
          }).then(function (r) {
            return r;
          }).catch(function(err){
            return err;
          });
        }
        return null;
      },
      getAppointmentSetListForSearch: function (startweek, endweek) {
        if(AppService.isTokenAvailable()){
          var _startweek = $filter('date')(new Date(startweek), 'yyyy-MM-dd');
          var _endweek = $filter('date')(new Date(endweek), 'yyyy-MM-dd');
          var url=AppService.apiPrefix+'api/appointments/getAppointmentSetListForSearch/'+_startweek+'/'+_endweek;
          return $http({
            url: url,
            method: "GET",
            headers: {
              'Content-Type': 'application/json',
              'Authorization':'Bearer '+AppService.getToken()
            }
          }).then(function (r) {
            return r;
          }).catch(function(err){
            return err;
          });
        }
        return null;
      }
    };
  });
