/**
 * Created by Shakeel Ahmad on 6/13/2017.
 */
angular.module('starter.services')
  .factory('TechAppointmentService', function ($http,$filter,AppService){
    return {
      getSaleLine: function(saleLineId){
        if(AppService.isTokenAvailable()){
          var url=AppService.apiPrefix+'api/techAppointments/getSaleLine/'+saleLineId;
          return $http({
            url: url,
            method: "GET",
            headers: {
              'Content-Type': 'application/json',
              'Authorization':'Bearer '+AppService.getToken()
            }
          }).then(function (r) {
            return r;
          }).catch(function(err){
            return err;
          });
        }
        return null;
      },
      getTechAppSetByClientAsync: function(clientId){
        if(AppService.isTokenAvailable()){
          var url=AppService.apiPrefix+'api/techAppointments/getTechAppSetByClientAsync/'+clientId;
          return $http({
            url: url,
            method: "GET",
            headers: {
              'Content-Type': 'application/json',
              'Authorization':'Bearer '+AppService.getToken()
            }
          }).then(function (r) {
            return r;
          }).catch(function(err){
            return err;
          });
        }
        return null;
      },
      techAppointments: function(techAppId){
        if(AppService.isTokenAvailable()){
          var url=AppService.apiPrefix+'api/techAppointments/'+techAppId;
          return $http({
            url: url,
            method: "GET",
            headers: {
              'Content-Type': 'application/json',
              'Authorization':'Bearer '+AppService.getToken()
            }
          }).then(function (r) {
            return r;
          }).catch(function(err){
            return err;
          });
        }
        return null;
      }
    };
  });
