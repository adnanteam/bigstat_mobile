/**
 * Created by Shakeel Ahmad on 6/13/2017.
 */
angular.module('starter.services')
  .factory('ProfileService', function ($http,$filter,AppService){
    return {
      getObjectiveForUser: function(id){
        if(AppService.isTokenAvailable()){
          var url=AppService.apiPrefix+'api/objectives/getObjectiveForUser/'+id;
          return $http({
            url: url,
            method: "GET",
            headers: {
              'Content-Type': 'application/json',
              'Authorization':'Bearer '+AppService.getToken()
            }
          }).then(function (r) {
            return r;
          }).catch(function(err){
            return err;
          });
        }
        return null;
      },
      getCallOperatorSellerAppChart: function(userProfileId,month){
        if(AppService.isTokenAvailable()){
          var url=AppService.apiPrefix+'api/profils/getCallOperatorSellerAppChart/'+userProfileId+'/'+month;
          return $http({
            url: url,
            method: "GET",
            headers: {
              'Content-Type': 'application/json',
              'Authorization':'Bearer '+AppService.getToken()
            }
          }).then(function (r) {
            return r;
          }).catch(function(err){
            return err;
          });
        }
        return null;
      },
      prizelist: function(userProfileId,month){
        if(AppService.isTokenAvailable()){
          var url=AppService.apiPrefix+'api/prizelist';
          return $http({
            url: url,
            method: "GET",
            headers: {
              'Content-Type': 'application/json',
              'Authorization':'Bearer '+AppService.getToken()
            }
          }).then(function (r) {
            return r;
          }).catch(function(err){
            return err;
          });
        }
        return null;
      },
      getLastAppData: function(){
        if(AppService.isTokenAvailable()){
          var url=AppService.apiPrefix+'api/profils/getLastApp/'+JSON.parse(localStorage.getItem('user-info')).id+'/5';
          return $http({
            url: url,
            method: "GET",
            headers: {
              'Content-Type': 'application/json',
              'Authorization':'Bearer '+AppService.getToken()
            }
          }).then(function (r) {
            return r;
          }).catch(function(err){
            return err;
          });
        }
        return null;
      },
      getLastAppForUsers: function(){
        if(AppService.isTokenAvailable()){
          var url=AppService.apiPrefix+'api/profils/getLastAppForUsers/'+JSON.parse(localStorage.getItem('user-info')).id+'/5';
          return $http({
            url: url,
            method: "GET",
            headers: {
              'Content-Type': 'application/json',
              'Authorization':'Bearer '+AppService.getToken()
            }
          }).then(function (r) {
            return r;
          }).catch(function(err){
            return err;
          });
        }
        return null;
      },
      getNbSalesCaByUser : function (startDate, lastDate) {
        if(AppService.isTokenAvailable()){
          var id= JSON.parse(localStorage.getItem('user-info')).id;
          var _startDate= $filter('date')(new Date(startDate), 'yyyy-MM-dd');
          var _lastDate= $filter('date')(new Date(lastDate), 'yyyy-MM-dd');
          var url=AppService.apiPrefix+'api/profils/getNbSalesCaByUsers/'+id+'/'+_startDate+'/'+_lastDate;
          return $http({
            url: url,
            method: "GET",
            headers: {
              'Content-Type': 'application/json',
              'Authorization':'Bearer '+AppService.getToken()
            }
          }).then(function (r) {
            return r;
          }).catch(function(err){
            return err;
          });
        }
        return null;
      }
    };
  });
