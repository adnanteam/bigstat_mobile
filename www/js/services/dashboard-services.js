/**
 * Created by Shakeel Ahmad on 6/13/2017.
 */
angular.module('starter.services')
  .factory('DashboardService', function ($http,$filter,AppService){
    return {
      getDashboardHeader: function(){
        if(AppService.isTokenAvailable()){
          var url=AppService.apiPrefix+'api/dashboards/getDashboardHeader';
          return $http({
            url: url,
            method: "GET",
            headers: {
              'Content-Type': 'application/json',
              'Authorization':'Bearer '+AppService.getToken()
            }
          }).then(function (r) {
            return r;
          }).catch(function(err){
            return err;
          });
        }
        return null;
      },
      getBranchObjective: function(){
        if(AppService.isTokenAvailable()){
          var url=AppService.apiPrefix+'api/dashboards/getBranchObjective';
          return $http({
            url: url,
            method: "GET",
            headers: {
              'Content-Type': 'application/json',
              'Authorization':'Bearer '+AppService.getToken()
            }
          }).then(function (r) {
            return r;
          }).catch(function(err){
            return err;
          });
        }
        return null;
      },
      getDashboardByMonth: function(month){
        if(AppService.isTokenAvailable()){
          var _month= $filter('date')(new Date(month), 'yyyy-MM-dd');
          var url=AppService.apiPrefix+'api/dashboards/getDashboardByMonth/'+_month;
          return $http({
            url: url,
            method: "GET",
            headers: {
              'Content-Type': 'application/json',
              'Authorization':'Bearer '+AppService.getToken()
            }
          }).then(function (r) {
            return r;
          }).catch(function(err){
            return err;
          });
        }
        return null;
      },
      getDashboardByMonthByUser: function(day){
        if(AppService.isTokenAvailable()){
          var _day = $filter('date')(new Date(day), 'yyyy-MM-dd');
          var url=AppService.apiPrefix+'api/dashboards/getDashboardByMonthByUser/'+
            _day+'/'+JSON.parse(localStorage.getItem('user-info')).id;
          return $http({
            url: url,
            method: "GET",
            headers: {
              'Content-Type': 'application/json',
              'Authorization':'Bearer '+AppService.getToken()
            }
          }).then(function (r) {
            return r;
          }).catch(function(err){
            return err;
          });
        }
        return null;
      },
      getDashboardByYear: function(year){
        if(AppService.isTokenAvailable()){
          var _year= $filter('date')(new Date(year), 'yyyy-MM-dd');
          var url=AppService.apiPrefix+'api/dashboards/getDashboardByYear/'+_year;
          return $http({
            url: url,
            method: "GET",
            headers: {
              'Content-Type': 'application/json',
              'Authorization':'Bearer '+AppService.getToken()
            }
          }).then(function (r) {
            return r;
          }).catch(function(err){
            return err;
          });
        }
        return null;
      },
      getDashboardByYearByUser: function(day){
        if(AppService.isTokenAvailable()){
          var _day = $filter('date')(new Date(day), 'yyyy-MM-dd');
          var url=AppService.apiPrefix+'api/dashboards/getDashboardByYearByUser/'+
            _day+'/'+JSON.parse(localStorage.getItem('user-info')).id;
          return $http({
            url: url,
            method: "GET",
            headers: {
              'Content-Type': 'application/json',
              'Authorization':'Bearer '+AppService.getToken()
            }
          }).then(function (r) {
            return r;
          }).catch(function(err){
            return err;
          });
        }
        return null;
      },
      getDashboardByDay: function(year){
        if(AppService.isTokenAvailable()){
          var _year= $filter('date')(new Date(year), 'yyyy-MM-dd');
          var url=AppService.apiPrefix+'api/dashboards/getDashboardByDay/'+_year;
          return $http({
            url: url,
            method: "GET",
            headers: {
              'Content-Type': 'application/json',
              'Authorization':'Bearer '+AppService.getToken()
            }
          }).then(function (r) {
            return r;
          }).catch(function(err){
            return err;
          });
        }
        return null;
      },
      getDashboardByDayByUser: function(day){
        if(AppService.isTokenAvailable()){
          var _day = $filter('date')(new Date(day), 'yyyy-MM-dd');
          var url=AppService.apiPrefix+'api/dashboards/getDashboardByUserByDay/'+
            _day+'/'+JSON.parse(localStorage.getItem('user-info')).id;
          return $http({
            url: url,
            method: "GET",
            headers: {
              'Content-Type': 'application/json',
              'Authorization':'Bearer '+AppService.getToken()
            }
          }).then(function (r) {
            return r;
          }).catch(function(err){
            return err;
          });
        }
        return null;
      },
      getDashBoardAppStatByWeekByUser: function(startday,endday){
        if(AppService.isTokenAvailable()){
          var _startday = $filter('date')(new Date(startday), 'yyyy-MM-dd');
          var _endday = $filter('date')(new Date(endday), 'yyyy-MM-dd');
          var url=AppService.apiPrefix+'api/dashboards/getDashBoardAppStatByWeekByUser'+
            '/'+JSON.parse(localStorage.getItem('user-info')).id+"/"+_startday+"/"+_endday;
          return $http({
            url: url,
            method: "GET",
            headers: {
              'Content-Type': 'application/json',
              'Authorization':'Bearer '+AppService.getToken()
            }
          }).then(function (r) {
            return r;
          }).catch(function(err){
            return err;
          });
        }
        return null;
      }
    };
  });
