/**
 * Created by Shakeel Ahmad on 6/13/2017.
 */
angular.module('starter.services')
  .factory('SaleService', function ($http,$filter,AppService){
    return {
      getNbSaleByMonthForYear: function (year) {
        if(AppService.isTokenAvailable()){
          var _date = $filter('date')(new Date(year), 'yyyy-MM-dd');
          var url=AppService.apiPrefix+'api/sales/getNbSaleByMonthForYear/'+_date;
          return $http({
            url: url,
            method: "GET",
            headers: {
              'Content-Type': 'application/json',
              'Authorization':'Bearer '+AppService.getToken()
            }
          }).then(function (r) {
            return r;
          }).catch(function(err){
            return err;
          });
        }
        return null;
      },
      getNbSaleByDayForMonth: function (month) {
        if(AppService.isTokenAvailable()){
          var _date = $filter('date')(new Date(month), 'yyyy-MM-dd');
          var url=AppService.apiPrefix+'api/sales/getNbSaleByDayForMonth/'+_date;
          return $http({
            url: url,
            method: "GET",
            headers: {
              'Content-Type': 'application/json',
              'Authorization':'Bearer '+AppService.getToken()
            }
          }).then(function (r) {
            return r;
          }).catch(function(err){
            return err;
          });
        }
        return null;
      },
      getNbSaleDoneByMonthForYear: function (month) {
        if(AppService.isTokenAvailable()){
          var _date = $filter('date')(new Date(month), 'yyyy-MM-dd');
          var url=AppService.apiPrefix+'api/sales/getNbSaleDoneByMonthForYear/'+_date;
          return $http({
            url: url,
            method: "GET",
            headers: {
              'Content-Type': 'application/json',
              'Authorization':'Bearer '+AppService.getToken()
            }
          }).then(function (r) {
            return r;
          }).catch(function(err){
            return err;
          });
        }
        return null;
      },
      getNbSaleDoneByDayForMonth: function (month) {
        if(AppService.isTokenAvailable()){
          var _date = $filter('date')(new Date(month), 'yyyy-MM-dd');
          var url=AppService.apiPrefix+'api/sales/getNbSaleDoneByDayForMonth/'+_date;
          return $http({
            url: url,
            method: "GET",
            headers: {
              'Content-Type': 'application/json',
              'Authorization':'Bearer '+AppService.getToken()
            }
          }).then(function (r) {
            return r;
          }).catch(function(err){
            return err;
          });
        }
        return null;
      },
      getSaleResumeSetForADayAsWeek: function (day) {
        if(AppService.isTokenAvailable()){
          var _date = $filter('date')(new Date(day), 'yyyy-MM-dd');
          var url=AppService.apiPrefix+'api/sales/getSaleResumeSetForADayAsWeek/'+_date;
          return $http({
            url: url,
            method: "GET",
            headers: {
              'Content-Type': 'application/json',
              'Authorization':'Bearer '+AppService.getToken()
            }
          }).then(function (r) {
            return r;
          }).catch(function(err){
            return err;
          });
        }
        return null;
      },
      getSaleResumeSetForADay: function (day) {
        if(AppService.isTokenAvailable()){
          var _date = $filter('date')(new Date(day), 'yyyy-MM-dd');
          var url=AppService.apiPrefix+'api/sales/getSaleResumeSetForADay/'+_date;
          return $http({
            url: url,
            method: "GET",
            headers: {
              'Content-Type': 'application/json',
              'Authorization':'Bearer '+AppService.getToken()
            }
          }).then(function (r) {
            return r;
          }).catch(function(err){
            return err;
          });
        }
        return null;
      },
      getSaleResumeSetForWeek: function (startweek,endweek) {
        if(AppService.isTokenAvailable()){
          var _start_week = $filter('date')(new Date(startweek), 'yyyy-MM-dd');
          var _end_week = $filter('date')(new Date(endweek), 'yyyy-MM-dd');
          var url=AppService.apiPrefix+'api/sales/getSaleResumeSetForWeek/'+_start_week+'/'+_end_week;
          return $http({
            url: url,
            method: "GET",
            headers: {
              'Content-Type': 'application/json',
              'Authorization':'Bearer '+AppService.getToken()
            }
          }).then(function (r) {
            return r;
          }).catch(function(err){
            return err;
          });
        }
        return null;
      }
    };
  });
