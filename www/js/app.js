// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'ngCordova', 'starter.controllers', 'starter.services'])
  .constant('_', window._)
  .run(function ($ionicPlatform) {
    $ionicPlatform.ready(function () {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);

      }
      if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }
    });
  })

  .config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

$ionicConfigProvider.tabs.position("bottom");
    $stateProvider
      .state('tab', {
        url: '/tab',
        abstract: true,
        templateUrl: 'templates/tabs.html'
      })
      .state('login', {
        url: '/login',
        templateUrl: 'templates/login.html',
        controller: 'LoginCtrl'
      })
      .state('tab.home', {
        url: '/home',
        views: {
          'tab-home': {
            templateUrl: 'templates/tab-home.html',
            controller: 'HomeCtrl'
          }
        }
      })
      .state('tab.graph', {
        url: '/graph',
        views: {
          'tab-graph': {
            templateUrl: 'templates/tab-graph.html',
            controller: 'GraphsCtrl'
          }
        }
      })
      .state('tab.event', {
        url: '/event',
        views: {
          'tab-event': {
            templateUrl: 'templates/tab-event.html',
            controller: 'EventCtrl'
          }
        }
      })

      .state('tab.calender', {
        url: '/calender',
        views: {
          'tab-calender': {
            templateUrl: 'templates/tab-calender.html',
            controller: 'CalenderCtrl'
          }
        }

      })
      .state('tab.equal', {
        url: '/equal',
        views: {
          'tab-equal': {
            templateUrl: 'templates/tab-equal.html',
            controller: 'EqualCtrl'
          }
        }
      })
      .state('appointments', {
        url: '/tab/event/appointments',
        templateUrl: 'templates/appointments.html',
        controller: 'AppointmentCtrl'
      });
      $urlRouterProvider.otherwise('/login');

  });
